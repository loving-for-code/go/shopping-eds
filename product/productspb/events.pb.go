// Code generated by protoc-gen-go. DO NOT EDIT.
// versions:
// 	protoc-gen-go v1.30.0
// 	protoc        (unknown)
// source: productspb/events.proto

package productspb

import (
	protoreflect "google.golang.org/protobuf/reflect/protoreflect"
	protoimpl "google.golang.org/protobuf/runtime/protoimpl"
	reflect "reflect"
	sync "sync"
)

const (
	// Verify that this generated code is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(20 - protoimpl.MinVersion)
	// Verify that runtime/protoimpl is sufficiently up-to-date.
	_ = protoimpl.EnforceVersion(protoimpl.MaxVersion - 20)
)

type ProductCreated struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id          string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Name        string  `protobuf:"bytes,3,opt,name=name,proto3" json:"name,omitempty"`
	Description string  `protobuf:"bytes,4,opt,name=description,proto3" json:"description,omitempty"`
	Price       float64 `protobuf:"fixed64,5,opt,name=price,proto3" json:"price,omitempty"`
}

func (x *ProductCreated) Reset() {
	*x = ProductCreated{}
	if protoimpl.UnsafeEnabled {
		mi := &file_productspb_events_proto_msgTypes[0]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductCreated) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductCreated) ProtoMessage() {}

func (x *ProductCreated) ProtoReflect() protoreflect.Message {
	mi := &file_productspb_events_proto_msgTypes[0]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductCreated.ProtoReflect.Descriptor instead.
func (*ProductCreated) Descriptor() ([]byte, []int) {
	return file_productspb_events_proto_rawDescGZIP(), []int{0}
}

func (x *ProductCreated) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ProductCreated) GetName() string {
	if x != nil {
		return x.Name
	}
	return ""
}

func (x *ProductCreated) GetDescription() string {
	if x != nil {
		return x.Description
	}
	return ""
}

func (x *ProductCreated) GetPrice() float64 {
	if x != nil {
		return x.Price
	}
	return 0
}

type ProductPriceChanged struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id    string  `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
	Delta float64 `protobuf:"fixed64,2,opt,name=delta,proto3" json:"delta,omitempty"`
}

func (x *ProductPriceChanged) Reset() {
	*x = ProductPriceChanged{}
	if protoimpl.UnsafeEnabled {
		mi := &file_productspb_events_proto_msgTypes[1]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductPriceChanged) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductPriceChanged) ProtoMessage() {}

func (x *ProductPriceChanged) ProtoReflect() protoreflect.Message {
	mi := &file_productspb_events_proto_msgTypes[1]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductPriceChanged.ProtoReflect.Descriptor instead.
func (*ProductPriceChanged) Descriptor() ([]byte, []int) {
	return file_productspb_events_proto_rawDescGZIP(), []int{1}
}

func (x *ProductPriceChanged) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

func (x *ProductPriceChanged) GetDelta() float64 {
	if x != nil {
		return x.Delta
	}
	return 0
}

type ProductDeleted struct {
	state         protoimpl.MessageState
	sizeCache     protoimpl.SizeCache
	unknownFields protoimpl.UnknownFields

	Id string `protobuf:"bytes,1,opt,name=id,proto3" json:"id,omitempty"`
}

func (x *ProductDeleted) Reset() {
	*x = ProductDeleted{}
	if protoimpl.UnsafeEnabled {
		mi := &file_productspb_events_proto_msgTypes[2]
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		ms.StoreMessageInfo(mi)
	}
}

func (x *ProductDeleted) String() string {
	return protoimpl.X.MessageStringOf(x)
}

func (*ProductDeleted) ProtoMessage() {}

func (x *ProductDeleted) ProtoReflect() protoreflect.Message {
	mi := &file_productspb_events_proto_msgTypes[2]
	if protoimpl.UnsafeEnabled && x != nil {
		ms := protoimpl.X.MessageStateOf(protoimpl.Pointer(x))
		if ms.LoadMessageInfo() == nil {
			ms.StoreMessageInfo(mi)
		}
		return ms
	}
	return mi.MessageOf(x)
}

// Deprecated: Use ProductDeleted.ProtoReflect.Descriptor instead.
func (*ProductDeleted) Descriptor() ([]byte, []int) {
	return file_productspb_events_proto_rawDescGZIP(), []int{2}
}

func (x *ProductDeleted) GetId() string {
	if x != nil {
		return x.Id
	}
	return ""
}

var File_productspb_events_proto protoreflect.FileDescriptor

var file_productspb_events_proto_rawDesc = []byte{
	0x0a, 0x17, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73, 0x70, 0x62, 0x2f, 0x65, 0x76, 0x65,
	0x6e, 0x74, 0x73, 0x2e, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x12, 0x09, 0x70, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x70, 0x62, 0x22, 0x6c, 0x0a, 0x0e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x43,
	0x72, 0x65, 0x61, 0x74, 0x65, 0x64, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01,
	0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x12, 0x0a, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x18, 0x03,
	0x20, 0x01, 0x28, 0x09, 0x52, 0x04, 0x6e, 0x61, 0x6d, 0x65, 0x12, 0x20, 0x0a, 0x0b, 0x64, 0x65,
	0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x18, 0x04, 0x20, 0x01, 0x28, 0x09, 0x52,
	0x0b, 0x64, 0x65, 0x73, 0x63, 0x72, 0x69, 0x70, 0x74, 0x69, 0x6f, 0x6e, 0x12, 0x14, 0x0a, 0x05,
	0x70, 0x72, 0x69, 0x63, 0x65, 0x18, 0x05, 0x20, 0x01, 0x28, 0x01, 0x52, 0x05, 0x70, 0x72, 0x69,
	0x63, 0x65, 0x22, 0x3b, 0x0a, 0x13, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x50, 0x72, 0x69,
	0x63, 0x65, 0x43, 0x68, 0x61, 0x6e, 0x67, 0x65, 0x64, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18,
	0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69, 0x64, 0x12, 0x14, 0x0a, 0x05, 0x64, 0x65, 0x6c,
	0x74, 0x61, 0x18, 0x02, 0x20, 0x01, 0x28, 0x01, 0x52, 0x05, 0x64, 0x65, 0x6c, 0x74, 0x61, 0x22,
	0x20, 0x0a, 0x0e, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x44, 0x65, 0x6c, 0x65, 0x74, 0x65,
	0x64, 0x12, 0x0e, 0x0a, 0x02, 0x69, 0x64, 0x18, 0x01, 0x20, 0x01, 0x28, 0x09, 0x52, 0x02, 0x69,
	0x64, 0x42, 0x80, 0x01, 0x0a, 0x0d, 0x63, 0x6f, 0x6d, 0x2e, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63,
	0x74, 0x70, 0x62, 0x42, 0x0b, 0x45, 0x76, 0x65, 0x6e, 0x74, 0x73, 0x50, 0x72, 0x6f, 0x74, 0x6f,
	0x50, 0x01, 0x5a, 0x1e, 0x73, 0x68, 0x6f, 0x70, 0x70, 0x69, 0x6e, 0x67, 0x2f, 0x70, 0x72, 0x6f,
	0x64, 0x75, 0x63, 0x74, 0x2f, 0x70, 0x62, 0x2f, 0x70, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x73,
	0x70, 0x62, 0xa2, 0x02, 0x03, 0x50, 0x58, 0x58, 0xaa, 0x02, 0x09, 0x50, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x70, 0x62, 0xca, 0x02, 0x09, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x70, 0x62,
	0xe2, 0x02, 0x15, 0x50, 0x72, 0x6f, 0x64, 0x75, 0x63, 0x74, 0x70, 0x62, 0x5c, 0x47, 0x50, 0x42,
	0x4d, 0x65, 0x74, 0x61, 0x64, 0x61, 0x74, 0x61, 0xea, 0x02, 0x09, 0x50, 0x72, 0x6f, 0x64, 0x75,
	0x63, 0x74, 0x70, 0x62, 0x62, 0x06, 0x70, 0x72, 0x6f, 0x74, 0x6f, 0x33,
}

var (
	file_productspb_events_proto_rawDescOnce sync.Once
	file_productspb_events_proto_rawDescData = file_productspb_events_proto_rawDesc
)

func file_productspb_events_proto_rawDescGZIP() []byte {
	file_productspb_events_proto_rawDescOnce.Do(func() {
		file_productspb_events_proto_rawDescData = protoimpl.X.CompressGZIP(file_productspb_events_proto_rawDescData)
	})
	return file_productspb_events_proto_rawDescData
}

var file_productspb_events_proto_msgTypes = make([]protoimpl.MessageInfo, 3)
var file_productspb_events_proto_goTypes = []interface{}{
	(*ProductCreated)(nil),      // 0: productpb.ProductCreated
	(*ProductPriceChanged)(nil), // 1: productpb.ProductPriceChanged
	(*ProductDeleted)(nil),      // 2: productpb.ProductDeleted
}
var file_productspb_events_proto_depIdxs = []int32{
	0, // [0:0] is the sub-list for method output_type
	0, // [0:0] is the sub-list for method input_type
	0, // [0:0] is the sub-list for extension type_name
	0, // [0:0] is the sub-list for extension extendee
	0, // [0:0] is the sub-list for field type_name
}

func init() { file_productspb_events_proto_init() }
func file_productspb_events_proto_init() {
	if File_productspb_events_proto != nil {
		return
	}
	if !protoimpl.UnsafeEnabled {
		file_productspb_events_proto_msgTypes[0].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductCreated); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_productspb_events_proto_msgTypes[1].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductPriceChanged); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
		file_productspb_events_proto_msgTypes[2].Exporter = func(v interface{}, i int) interface{} {
			switch v := v.(*ProductDeleted); i {
			case 0:
				return &v.state
			case 1:
				return &v.sizeCache
			case 2:
				return &v.unknownFields
			default:
				return nil
			}
		}
	}
	type x struct{}
	out := protoimpl.TypeBuilder{
		File: protoimpl.DescBuilder{
			GoPackagePath: reflect.TypeOf(x{}).PkgPath(),
			RawDescriptor: file_productspb_events_proto_rawDesc,
			NumEnums:      0,
			NumMessages:   3,
			NumExtensions: 0,
			NumServices:   0,
		},
		GoTypes:           file_productspb_events_proto_goTypes,
		DependencyIndexes: file_productspb_events_proto_depIdxs,
		MessageInfos:      file_productspb_events_proto_msgTypes,
	}.Build()
	File_productspb_events_proto = out.File
	file_productspb_events_proto_rawDesc = nil
	file_productspb_events_proto_goTypes = nil
	file_productspb_events_proto_depIdxs = nil
}
