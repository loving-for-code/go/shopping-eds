package handlers

import (
	"shopping/internal/ddd"
	"shopping/product/internal/domain"
)

func RegisterManagementHandler(managementHandlers ddd.EventHandler[ddd.AggregateEvent], domainSubscriber ddd.EventSubscriber[ddd.AggregateEvent]) {
	domainSubscriber.Subscribe(managementHandlers,
		domain.ProductCreatedEvent,
		domain.ProductDeletedEvent,
		domain.ProductInscreasedPriceEvent,
		domain.ProductDescreasedPriceEvent,
	)
}
